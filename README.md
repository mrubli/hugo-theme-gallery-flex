# Hugo theme: Gallery Flex

A very simple and slightly less opinionated fork of the great [Hugo Gallery theme](https://github.com/nicokaiser/hugo-theme-gallery/).

## Enhancements

Differences from the original Gallery theme include:

**Functionality:**

* Page content (the markup part of `index.md`) is rendered between the description and the photos.
  This allows for more detailed album introduction text.
* Footer content (contained in an album directory's `footer.md`) is rendered below the photos.
  This allows for more detailed album descriptions, links, maps, etc.
* Added the ability to highlight albums published in the last _N_ days.
* Allow customized generation of the copyright notice in the site footer.

**Convenience:**

* Album names are auto-generated from the directory name if `index.md` is empty.
* Album list names are auto-generated from the directory name if omitted in `_index.md`.
* Always render the album description, even if no explicit title is given.

**Design:**

* The _Inter Roman_ font has been replaced with a lighter weight of _Switzera_ for a more modern look. (Hopefully this will become more easily customizable in the future.)

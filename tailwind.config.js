// This is a copy of hugo-theme-gallery/tailwind.config.js but with a different font.
// See: https://github.com/nicokaiser/hugo-theme-gallery/issues/17

const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ["./hugo_stats.json", "./layouts/**/*.html"],
  darkMode: "class",
  experimental: {
    optimizeUniversalDefaults: true,
  },
  safelist: [
    // Button used by hugo-mod-leaflet
    'py-2.5',
    'px-5',
    'me-2',
    'mb-2',
    'text-sm',
    'font-medium',
    'rounded-lg',
    'border',
    'focus:outline-none',
    'focus:z-10',
    'focus:ring-4',
    'dark:focus:ring-gray-700',
    'dark:bg-gray-800',
    'dark:text-gray-400',
    'dark:border-gray-600',
    'dark:hover:text-white',
    'dark:hover:bg-gray-700',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Switzera", ...defaultTheme.fontFamily.sans],
      },
      screens: {
        "3xl": "1792px",
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            // This is a workaround for headings in markdown appearing black in the dark theme:
            '--tw-prose-headings': 'var(--tw-prose-invert-headings)',
            // The Hugo gallery theme uses semibold headings. Imitate that for markdown headings:
            h1: {
              'font-weight': 500,
            },
            h2: {
              'font-weight': 500,
            },
            h3: {
              'font-weight': 500,
            },
          },
        },
      }),
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
